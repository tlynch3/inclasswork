/**********************************************
* File: fact.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf

/********************************************
* Function Name  : factorial
* Pre-conditions : unsigned int i
* Post-conditions: double
*  
* Calculates i!
********************************************/
double factorial(unsigned int i){
	
	// Error Case 
	if(i < 0){
		std::cout << "Please enter a positive number" << std::endl;
		exit(-1);
	}
	
	std::cout << "Recursive call for " << i << std::endl;
	
	// Base case i = 0
	if(i == 0){
		std::cout << "Base Case found!" << std::endl;
		std::cout << "Base Case Result: " << i << std::endl;
		return 1;
	}
	
	// Recursive case
	double ret = i*factorial(i-1);
	
	// Printed to show students
	std::cout << "Recursive Result: " << ret << std::endl;
	return ret;	// return i * factorial(i-1);
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* Main driver for factorial fundamental
* Requires a valid positive integer as argv[1]
********************************************/
int main(int argc, char** argv){

	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}
	
	// stringstream used for the conversion initialized with the contents of argv[1]
	int factNum;
	std::stringstream convert(argv[1]);

	//give the value to factNum using the characters in the string
	if ( !(convert >> factNum) ){
		std::cout << "Not a valid double" << std::endl;
		exit(-1);
	}

	// Call the recursive function
	std::cout << "Recursive Final Result: " << factorial(factNum) << std::endl;

	return 0;

}
